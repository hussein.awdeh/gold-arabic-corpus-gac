# Gold Arabic Corpus GAC

     La langue Arabe est considérée comme une des six langues officielles des nations unies depuis 1973. C’est la langue du Saint Coran, et elle est parlée par plus de 273 millions de personnes dans le monde. Les dialectes arabes se composent de plusieurs branches : le classique (langue du Coran), le standard moderne (utilisé dans les journaux, les livres...) et les dialectes locaux (varient considérablement d’un pays à l’autre). Mais malheureusement, il existe peu de travaux ont été réalisés dans le domaine de l’analyse syntaxique automatique des mots arabe standard par rapport aux autre langues et surtout les langues européennes.

     Malgré tout ce qui précède et le besoin croissant de corpus arabe, le corpus arabe reste déficient pour soutenir diverses recherches linguistiques arabes. Par exemple, la majorité des corpus arabes sont limités dans les sources, les types et les genres ou même ne sont pas disponibles gratuitement. La langue arabe a une morphologie spéciale, où un mot est composé d’une racine à laquelle on peut ajouter un ou plusieurs préfixes, et/ou un ou plusieurs suffixes, et chaque combinaison peut donner un sens différent. Cette variabilité rend la langue arabe plus complexe par rapport aux autres langues, et par conséquent la segmentation des mots arabe est devenue un problème à résoudre par les informaticiens en ayant recours à différentes techniques.
 
     Une analyse automatique d’un texte arabe entier sans le fait de le segmenter en phrases conduit à des résultats erronés. De même, une mauvaise segmentation automatique de texte en phrases peut conduire à des erreurs supplémentaires du traitement automatique de ce texte. D’où l’objectif de cette thèse est d’essayer de segmenter automatiquement chaque mot arabe à part en éliminant le préfixe et le suffixe correctement afin de pouvoir extraire le morphème qui est l’unité minimale de signification. 
 
     L’analyse syntaxique de la morphologie consiste à segmenter les affixes d'un mot. Ainsi, tous les préfixes et les suffixes attachés à la tige deviennent séparés par un espace. Tenant cet exemple de segmentation morphologique :

                         ومكتبته = و مكتبة ه

     On remarque que le suffixe ه (h - seconde personne masculin) est séparé du mot.
  
     Le but essentiel est de créer un corpus de l’or (gold corpus), en déterminant premièrement les règles de grammaire de la langue arabe, afin de bien segmenter les mots. D’où la nécessité de développer un guide de segmentation des mots arabes qui aident à parvenir à un gold corpus standard et fiable.
   
     Pour cela, comme première étape, j’ai déterminé un guide segmentation des mots arabes qui regroupe les grammaires utilisées en langage arabe pour nous aider à bien segmenter les mots arabes dans notre nouvel corpus. Et Grace à notre corpus et à notre guide de segmentation qu’on a développé, et a différentes étapes de standardisation, la première version de notre gold arabic corpus est née dont le but de mesurer la performance (f-mesure, précision, recall) du résultat de segmentation (Validation) faite pour faire valider edt comparer les resultats des differents outils de segmentation des mots arbes telsque Stanford NLP, NLTK, Farasa, et autres.
   
     Notre corpus est divisé en deux parties :
          1- Corpus corrigé manuellement, constitue 52 fishiers de 28533 mots segmentés correctement.
          2- Corpus segmenté mais non corrigé manuellement, constitue 20240 fishiers segmentés automatiquement.
          
     Note : pour plus d'informations, n'hesitez pas à me connecter hussein.awdi85@gmail.com

